package com.example.nakatsu.alps;

import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DeviceListAdapter extends BaseAdapter {
	Context context;
	LayoutInflater layoutInflater = null;
	List<ALPSDevice> deviceList;

	public DeviceListAdapter(Context context){
		this.context = context;
		this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.deviceList = new ArrayList<>();
	}

	@Override
	public int getCount() {
		return deviceList.size();
	}

	@Override
	public Object getItem(int position) {
		return deviceList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return deviceList.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = layoutInflater.inflate(R.layout.adopter_device,parent,false);

		((TextView)convertView.findViewById(R.id.DeviceName)).setText(deviceList.get(position).getName());
		((TextView)convertView.findViewById(R.id.DeviceNameSub)).setText(deviceList.get(position).getAddress());
		((Switch)convertView.findViewById(R.id.UsingSwitch)).setChecked(deviceList.get(position).getUsing());

		return convertView;
	}

	public void addDevice(ALPSDevice device) {
		deviceList.add(device);
	}

	public List<ALPSDevice> getList() {
		return deviceList;
	}
}
