package com.example.nakatsu.alps;

import android.bluetooth.BluetoothDevice;

public class ALPSDevice {
	private long id;
	private String name;
	private String address;
	private boolean using;

	public ALPSDevice(BluetoothDevice device){
		if (device.getName() != null) {
			setName(device.getName());
		} else {
			setName(device.getAddress());
		}
		setId(device.hashCode());
		setAddress(device.getAddress());
		setUsing(false);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean getUsing() {
		return using;
	}

	public void setUsing(boolean using) {
		this.using = using;
	}
}
