package com.example.nakatsu.alps;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

	private int REQUEST_PERMISSION;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Context context = getApplicationContext();
		LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		final boolean gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
		final boolean wifiEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		if (!gpsEnabled && !wifiEnabled) {
			Toast toast = Toast.makeText(this,
					"位置情報を許可してください", Toast.LENGTH_SHORT);
			toast.show();
		} else {
			versionCheck();
		}

	}

	private void versionCheck() {
		if(Build.VERSION.SDK_INT >= 23){
			checkPermission();
		}
		else{
			locationActivity();
		}
	}


	// 位置情報許可の確認
	public void checkPermission() {
		// 既に許可している
		if (ActivityCompat.checkSelfPermission(this,
			Manifest.permission.ACCESS_FINE_LOCATION)==
			PackageManager.PERMISSION_GRANTED){

			locationActivity();
		}
		// 拒否していた場合
		else{
			requestLocationPermission();
		}
	}

	// 許可を求める
	private void requestLocationPermission() {
		if (ActivityCompat.shouldShowRequestPermissionRationale(this,
				Manifest.permission.ACCESS_FINE_LOCATION)) {
			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
					REQUEST_PERMISSION);

		} else {
			Toast toast = Toast.makeText(this,
					"許可されないとアプリが実行できません", Toast.LENGTH_SHORT);
			toast.show();

			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.ACCESS_FINE_LOCATION,},
					REQUEST_PERMISSION);

		}
	}

	// 結果の受け取り
	@Override
	public void onRequestPermissionsResult(
			int requestCode,
			@NonNull String[] permissions,
			@NonNull int[] grantResults) {

		if (requestCode == REQUEST_PERMISSION) {
			// 使用が許可された
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				locationActivity();

			} else {
				// それでも拒否された時の対応
				Toast toast = Toast.makeText(this,
						"これ以上なにもできません", Toast.LENGTH_SHORT);
				toast.show();
			}
		}
	}

	// Intent でLocation
	private void locationActivity() {
		Intent intent = new Intent(this, StartActivity.class);
		startActivity(intent);
	}
}
