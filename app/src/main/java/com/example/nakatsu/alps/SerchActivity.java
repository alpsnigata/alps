package com.example.nakatsu.alps;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


public class SerchActivity extends AppCompatActivity {

	private ScanSettings scanSettings;
	private BluetoothLeScanner bluetoothLeScanner;
	private DeviceListAdapter alpsAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_serch);

		BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
		bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
		scanSettings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_BALANCED).build();
		bluetoothLeScanner.startScan(null, scanSettings, scanCallback);

		ListView listView = (ListView) findViewById(R.id.DeviceList);
		alpsAdapter = new DeviceListAdapter(this);
		listView.setAdapter(alpsAdapter);
		final SerchActivity thisPage = this;

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ALPSDevice device = (ALPSDevice) alpsAdapter.getItem(position);
				if (view.getId() == R.id.DeviceName || view.getId() == R.id.DeviceNameSub) {
					Intent intent = new Intent(thisPage, DeviceActivity.class);
					intent.putExtra("devicePosition", position);
					startActivity(intent);
				} else if (view.getId() == R.id.DeviceName || view.getId() == R.id.DeviceNameSub) {
					device.setUsing(!device.getUsing());
				}
			}
		});
	}

	private ScanCallback scanCallback = new ScanCallback() {
		@Override
		public void onScanResult(int callbackType, ScanResult result) {
			super.onScanResult(callbackType, result);

			BluetoothDevice device = result.getDevice();
			boolean newDevice = true;
			for (ALPSDevice device2 : alpsAdapter.getList() ) {
				if(device.getAddress().equals(device2.getAddress())){
					newDevice = false;
					break;
				}
			}
			if (newDevice) {
				ALPSDevice alpsDevice = new ALPSDevice(device);
				alpsAdapter.addDevice(alpsDevice);
				alpsAdapter.notifyDataSetChanged();
			}

			int rssi = result.getRssi();
		}
	};
}
